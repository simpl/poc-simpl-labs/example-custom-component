package it.eng.simpl.labs.poc.component.model;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Builder
public class TestModel {
	private Long id;
	private String title;
	private String text;
}
