package it.eng.simpl.labs.poc.component.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import it.eng.simpl.labs.poc.component.model.TestModel;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/custom-component")
@Slf4j
public class TestController {
	
	private static final String CUSTOM_COMPONENT = "/custom-component";
	
	private TestModel storedMessage = null;

	@GetMapping
	@Operation(summary = "Get all Test Model", description = "Get all Test Model")
	@ApiResponse(responseCode = "200", description = "List of Test Model", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE) })
	public List<TestModel> findAll() {
		
		List<TestModel> testModelList = Arrays.asList(TestModel.builder().id(1L).title("Hello").title("Test").build());
				
		log.info("GET {} - response: {}",  CUSTOM_COMPONENT, testModelList);

		return testModelList;
		

	}
	
	@GetMapping(value = "/read-mock")
	@Operation(summary = "Get mocked Test Model", description = "Get mocked Test Model")
	@ApiResponse(responseCode = "200", description = "Action successfully executed", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE) })
	public TestModel readBasic() {
		
		TestModel mockMessage = TestModel.builder().id(1L).title("Title1").text("Text1").build();
								
		log.info("GET {} - response: {}",  CUSTOM_COMPONENT+"/read-mock", mockMessage);
		
		return mockMessage;
	}
	
	@GetMapping(value = "/test-read-mock")
	@Operation(summary = "Get another mocked Test Model", description = "Get another mocked Test Model")
	@ApiResponse(responseCode = "200", description = "Action successfully executed", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE) })
	public TestModel readTest() {
		
		TestModel mockMessage = TestModel.builder().id(1L).title("CorrectTitleTest").text("CorrectTextTest").build();
								
		log.info("GET {} - response: {}",  CUSTOM_COMPONENT+"/test-read-mock", mockMessage);
		
		return mockMessage;
	}
		
		
	@PostMapping(value = "/save")
	@Operation(summary = "Save Test Model", description = "Save Test Model")
	@ApiResponse(responseCode = "200", description = "Action successfully executed", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE) })
	public TestModel save(@RequestBody TestModel inputMessage) {
				
		log.info("POST {} - body: [{}]", CUSTOM_COMPONENT+"/save", sanitizeForLogging(inputMessage.toString()));
		
		this.storedMessage = inputMessage;
		
		log.info("POST {} - response: {}", CUSTOM_COMPONENT+"/save", sanitizeForLogging(storedMessage.toString()));

		return storedMessage;
	}
	
	
	@GetMapping(value = "/read")
	@Operation(summary = "Get Test Model", description = "Get Test Model")
	@ApiResponse(responseCode = "200", description = "Action successfully executed", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE) })
	public TestModel read() {
								
		log.info("GET {} - response: {}",  CUSTOM_COMPONENT+"/read", sanitizeForLogging(this.storedMessage.toString()));
		
		return this.storedMessage;
	}
	
    @PostMapping(value = "/save-sleep")
    @Operation(summary = "Save Test Model", description = "Save Test Model with simulated delay response")
    @ApiResponse(responseCode = "200", description = "Action successfully executed", content = {
            @Content(mediaType = MediaType.APPLICATION_JSON_VALUE) })
    public TestModel saveSleep(@RequestBody TestModel inputMessage, @RequestParam(defaultValue = "10") @Parameter Integer sleep) {
        
		log.info("POST {} - param: [sleep={}] - body: [{}]", CUSTOM_COMPONENT+"/save-sleep", sleep, sanitizeForLogging(inputMessage.toString()));

        
        this.storedMessage = inputMessage;
        
        for(int i = 1; i<sleep; i++) {
        	
            try {
        		log.info("POST {} - executing sleep {}/{}", CUSTOM_COMPONENT+"/save-sleep", i, sleep);
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
				Thread.currentThread().interrupt();
            }
        }
        
		log.info("POST {} - response: {}", CUSTOM_COMPONENT+"/save-sleep", sanitizeForLogging(storedMessage.toString()));

        return storedMessage;
    }
    
    public void setStoredMessage(TestModel storedMessage) {
    	this.storedMessage=storedMessage;
    }
    
    private String sanitizeForLogging(String input) {
        if (input == null) {
            return null;
        }
        return input.replaceAll("[\\r\\n]", "_");
    }
}
