package it.eng.simpl.labs.poc.component;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExampleComponentApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExampleComponentApplication.class, args);
	}

}
