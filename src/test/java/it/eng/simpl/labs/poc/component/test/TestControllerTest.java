
package it.eng.simpl.labs.poc.component.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.fasterxml.jackson.databind.ObjectMapper;

import it.eng.simpl.labs.poc.component.controller.TestController;
import it.eng.simpl.labs.poc.component.model.TestModel;

@ExtendWith(MockitoExtension.class)
public class TestControllerTest {

	@InjectMocks
	private TestController testController;

	@BeforeEach
	public void setup() {
		MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
	}

	@Test
	public void testFindAll() throws Exception {
		List<TestModel> responseEntity = testController.findAll();
        assertThat(responseEntity).isNotNull().hasSizeGreaterThan(0);
	}
	
	@Test
	public void testReadBasic() throws Exception {
		TestModel testModel = testController.readBasic();
        assertThat(testModel).isNotNull().hasFieldOrPropertyWithValue("title", "Title1");
	}
	
	@Test
	public void testReadTest() throws Exception {
		TestModel testModel = testController.readTest();
        assertThat(testModel).isNotNull().hasFieldOrPropertyWithValue("title", "CorrectTitleTest");
	}
	
	@Test
	public void testSave() throws Exception {
		TestModel testModel=TestModel.builder().title("testTitle").build();
		TestModel testModelReturned = testController.save(testModel);
        assertThat(testModelReturned).isNotNull().hasFieldOrPropertyWithValue("title", testModel.getTitle());
	}
	
	@Test
	public void testReadAfterSave() throws Exception {
		TestModel testModel=TestModel.builder().title("testTitleSaved").build();
		testController.setStoredMessage(testModel);
		TestModel testModelReturned = testController.read();
        assertThat(testModelReturned).isNotNull().hasFieldOrPropertyWithValue("title", testModel.getTitle());
	}
	
	@Test
	public void testSaveSleep() throws Exception {
		TestModel testModel=TestModel.builder().title("testTitle").build();
		TestModel testModelReturned = testController.saveSleep(testModel,3);
        assertThat(testModelReturned).isNotNull().hasFieldOrPropertyWithValue("title", testModel.getTitle());
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}