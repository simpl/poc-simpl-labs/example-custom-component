FROM eclipse-temurin:21-jdk
CMD mkdir /app
COPY target/*.jar /app/poc-component.jar
EXPOSE 8081
ENTRYPOINT ["java", "-jar", "/app/poc-component.jar"]